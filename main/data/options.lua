game = game or {}

game.options = 
{
	video =
	{
		resizable	= true, 
		vsync		= false,
		fullscreen  = false,
		centered	= true,
		srgb		= true,
		borderless  = false,
		highdpi     = false,

		minwidth	= 800, 
		minheight	= 600,
		fsaa		= 0,

		fullscreentype	= "normal", --display
	},
	audio = 
	{
		gui       = false,
		music     = true,
		ambient   = true,
		mechanism = true,
	},
	binds = 
	{
		inventory	= "i",
		drop		= "q",
		pick		= "r",
		use			= "e",
		flashlight	= "f",
		mainmenu    = "escape"
	}

}

