Color = {}

function Color:Red()

	return self[1]

end

function Color:Green()

	return self[2]

end

function Color:Blue()

	return self[3]

end

function Color:Alpha()

	return self[4]

end

function Color:SetAlpha(_a)
	
	self[4] = _a

end

local COLOR_META = {}

COLOR_META.__index = function(key)
	
	return COLOR_META[key]

end

COLOR_META.__unm = function(_a)
	return color(
		-_a[1],
		-_a[2],
		-_a[3],
		 _a[4]
	)
end

COLOR_META.__add = function(_a,_b)
	return color(
		_a[1] + _b[1],
		_a[2] + _b[2],
		_a[3] + _b[3],
		_a[4]
	)
end

COLOR_META.__sub = function(_a,_b)
	return color(
		_a[1] - _b[1],
		_a[2] - _b[2],
		_a[3] - _b[3],
		_a[4]
	)
end

COLOR_META.__mul = function(_a,_b)
	if type(_b) == "number" then

		return color(
			_a[1] * _b,
			_a[2] * _b,
			_a[3] * _b,
			_a[4]
		)

	else

		return color(
			_a[1] * _b[1],
			_a[2] * _b[2],
			_a[3] * _b[3],
			_a[4]
		)

	end
end

COLOR_META.__div = function(_a,_b)

	if type(_b) == "number" then
		
		return color(
			_a[1] / _b,
			_a[2] / _b,
			_a[3] / _b
		)

	else

		return color(
			_a[1] / _b[1],
			_a[2] / _b[2],
			_a[3] / _b[3]
		)

	end

end

function Color:clamp(_var, _min, _max)
	
	local _build = {}

	for _id = 1, 4 do

		if _var[_id] >= _min[_id] and _var[_id] <= _max[_id] then 

			_build[_id] = _var[_id]

		elseif _var[_id] < _min[_id] then 

			_build[_id] = _min[_id]

		elseif _var[_id] > _max[_id] then

			_build[_id] = _max[_id]

		end

	end

	return color(_build[1], _build[2], _build[3], _build[4])

end

function Color:ToHSV(h, s, v)

    if s <= 0 then return color(v, v, v) end

    h, s, v = h/256*6, s/255, v/255

    local c = v*s
    local x = (1-math.abs((h%2)-1))*c
    local m,r,g,b = (v-c), 0,0,0

    if     h < 1 then r,g,b = c,x,0
    elseif h < 2 then r,g,b = x,c,0
    elseif h < 3 then r,g,b = 0,c,x
    elseif h < 4 then r,g,b = 0,x,c
    elseif h < 5 then r,g,b = x,0,c
    else              r,g,b = c,0,x
    end 
    return color((r+m)*255,(g+m)*255,(b+m)*255)
end

function color(_r,_g,_b,_a)

	local result = {}
	
	table.MergeAdvanced(result, Color)
	setmetatable(result, COLOR_META)

	result[1] = _r or 255
	result[2] = _g or 255
	result[3] = _b or 255
	result[4] = _a or 255
	
	return result

end