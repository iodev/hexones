game = game or {}
game.gui = 
{
	menu = {},
	list = {},
	crnt = "",

}

require "lib/gui/gui_sub_items"

local function menu_builder(_name, _buttons)
	
	if not game.gui.list[_name] then 

		game.gui.list[_name] = {}
		game.gui.list[_name].is_opened   = false
		game.gui.list[_name].is_finished = true
		game.gui.list[_name].idle        = 0

	end
	
	local t_menu = game.gui.list[_name]
    
	if t_menu.is_finished then

		t_menu.is_opened = not t_menu.is_opened
		
	end
	
	if t_menu.is_opened and t_menu.is_finished then
		
		t_menu.is_finished = false
		
		game.gui.crnt = _name

		local _p = 48

		t_menu.menu = game.vgui.Create("base_panel")

		t_menu.menu:SetSize(330, 768)
		t_menu.menu:SetColor(color(50,50,50,0))
		t_menu.menu.name = _name

		function t_menu.menu:OnThink()

			if t_menu.idle < 100 then

				t_menu.idle = t_menu.idle + 0.5
				t_menu.menu.color:SetAlpha(t_menu.idle)

			elseif not t_menu.is_finished then
				
				--MOVABLE CURSOR
				local _button = game.vgui.Create("base_panel", t_menu.menu)
				
				_button:SetSize(10, _p)
				_button:SetPos(320, -_button.orig_size.y)
				_button:SetColor(color(0,155,255,200))
				_button.prev_y     = -_button.orig_size.y
				_button.move_y     = 12	

				function _button:OnThink()  

					local goto_y = self.prev_y+(self.move_y-self.prev_y)/15
					local dist   = math.abs(self.move_y-goto_y)
					self.prev_y  = goto_y
					
					self:SetPos(self.orig_pos.x, goto_y)				
					self:SetColor(color(0, 155, 255-dist))
				
				end
				--MOVABLE CURSOR				

				for _id = 1, #_buttons do

					local _item   = _buttons[_id] 
					local _button = game.vgui.Create("base_button", t_menu.menu)

					_button:SetSize(256, _p)
					_button:SetPos(-_button.orig_size.x, (12*_id + _button.orig_size.y * (_id-1)))
					_button:SetColor(color(150,150,150,200), color(200,200,200,200), color(0,155,255,200))
					_button.is_enabled = _item._status
					_button.prev_x = -_button.orig_size.x
					_button.move_x = 12

					game.vgui.Create("base_text", _button)

					_button.child[1]:SetPos(0, 0)
					_button.child[1]:SetText(_item._text)
					_button.child[1]:SetFont("Menu")
					_button.child[1]:SetAlign(0, 0.2)				

					function _button:OnMouseEntered() self.move_x = 48 t_menu.menu.child[1].move_y = self.orig_pos.y end
					function _button:OnMousePressed() self.move_x = 36 end
					function _button:OnMouseExited()  self.move_x = 12 end
					function _button:OnThink()  

						local goto_x = self.prev_x+(self.move_x-self.prev_x)/15
						self.prev_x  = goto_x
						
						self:SetPos(goto_x, self.orig_pos.y)				

					end

					--function _button:OnMouseReleased() love.event.push(_item._event) end
					function _button:OnMouseReleased() 

						local _item0, _item1 = _item._event[1], _item._event[2]

						if _item0 then _item0() end
						if _item1 then _item1() end

					end

					t_menu.is_finished = true

				end

			end

		end

	elseif t_menu.is_finished then

		t_menu.is_finished = false

		for _id = 2, #t_menu.menu.child do
				
			local _item = t_menu.menu.child[_id]

			_item.move_x = -(_item.orig_size.x+32)

			function _item:OnMouseEntered() end
			function _item:OnMousePressed() end
			function _item:OnMouseReleased() end
			function _item:OnMouseExited() end

		end

		t_menu.menu.child[1].move_y = -(t_menu.menu.child[1].orig_size.y)

		function t_menu.menu:OnThink()

			if t_menu.idle > 0 then

				t_menu.idle = t_menu.idle - 0.5
				t_menu.menu.color:SetAlpha(t_menu.idle)

			elseif not t_menu.is_finished then

				t_menu.menu:Remove()

				t_menu.is_finished = true

			end

		end
		
	end

end

local function use_menu(_name)

	if game.gui.menu[_name] then

		menu_builder(_name, game.gui.menu[_name]._buttons)

	end

end

local function add_menu(_object)

	game.gui.menu[_object._name] = _object

end

game.gui.Use = function(_name)
	
	use_menu(_name)

end

--MAIN MENU
add_menu(
	{
		_name = "main_menu",
		_buttons = 
		{
			{
				_text   = "Game",
				_status = true,
				_event  = {function() use_menu("game_menu") end, function() use_menu("main_menu") end}
			},
			{
				_text   = "Options",
				_status = true,
				_event  = {function() use_menu("options_menu") end, function() use_menu("main_menu") end}
			},
			{
				_text   = "Statistic",
				_status = true,
				_event  = {function() end, function() end}
			},
			{
				_text   = "Achievments",
				_status = true,
				_event  = {function() end, function() end}
			},
			{
				_text   = "Exit",
				_status = true,
				_event  = {function() use_menu("exit_menu") end, function() use_menu("main_menu") end}
			}
		}
	})

--GAME MENU
add_menu(
	{
		_name = "game_menu",
		_buttons = 
		{
			{
				_text   = "New",
				_status = true,
				_event  = {}
			},
			{
				_text   = "Load",
				_status = true,
				_event  = {}
			},
			{
				_text   = "Save",
				_status = false,
				_event  = {}
			},
			{
				_text   = "Back",
				_status = true,
				_event  = {function() use_menu("game_menu") end, function() use_menu("main_menu") end}
			}
		}
	})

--OPTIONS MENU
add_menu(
	{
		_name = "options_menu",
		_buttons = 
		{
			{
				_text   = "Keyboard",
				_status = true,
				_event  = {}
			},
			{
				_text   = "Mouse",
				_status = true,
				_event  = {}
			},
			{
				_text   = "Video",
				_status = true,
				_event  = {}
			},
			{
				_text   = "Audio",
				_status = true,
				_event  = {function() game.gui.audio_opions() end}
			},
			{
				_text   = "Debug",
				_status = true,
				_event  = {function() end}
			},
			{
				_text   = "Back",
				_status = true,
				_event  = {function() use_menu("options_menu") end, function() use_menu("main_menu") end}
			}
		}
	})

--EXIT MENU
add_menu(
	{
		_name = "exit_menu",
		_buttons = 
		{
			{
				_text   = "Exit",
				_status = true,
				_event  = {function() love.event.quit( ) end}
			},
			{
				_text   = "Back",
				_status = true,
				_event  = {function() use_menu("exit_menu") end, function() use_menu("main_menu") end}
			}
		}
	})









