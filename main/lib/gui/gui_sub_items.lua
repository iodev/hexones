game.gui.audio_opions = function()

	if not game.gui.list["audio_options"] then 

		game.gui.list["audio_options"] = {} 

	end

	local _menu = game.gui.list["audio_options"]

	if not _menu.is_opened then

		local _text, _button, _slider, _checkbox

		_menu.is_opened = true

		_menu.item = game.vgui.Create("base_window")
	    _menu.item:SetPos(400,100)
	    _menu.item:SetSize(600,400)
	    _menu.item:SetDraggable(true, 500, 16)

	    _text = game.vgui.Create("base_text",_menu.item)
	    _text:SetPos(0,-2)
	    _text:SetAlign(0)
	    _text:SetText("///Audio options///")
	    _text:SetColor(color(0,0,0))

	    _button = game.vgui.Create("base_button",_menu.item)
	    _button:SetPos(500,0)
	    _button:SetSize(100,16)
		
		function _button:OnMouseReleased()

			_menu.is_opened = false
			self.owner:Remove()

		end

		_text = game.vgui.Create("base_text",_button)

	    _text:SetPos(0,-2)
	    _text:SetAlign(0)
	    _text:SetText("///close///")
	    _text:SetColor(color(0,0,0))

	    _checkbox = game.vgui.Create("base_checkbox",_menu.item)
	    _checkbox:SetPos(4,20)
	    _checkbox:SetSize(32,32)
	    _checkbox.text:SetText("///music")
	    _checkbox.button.tggl_status = game.options.audio.music
	    function _checkbox.button:OnMouseReleased() game.options.audio.music = self.tggl_status end

	    _checkbox = game.vgui.Create("base_checkbox",_menu.item)
	    _checkbox:SetPos(4,56)
	    _checkbox:SetSize(32,32)
	    _checkbox.text:SetText("///ambient")
   	    _checkbox.button.tggl_status = game.options.audio.ambient
	    function _checkbox.button:OnMouseReleased() game.options.audio.ambient = self.tggl_status end

	    _checkbox = game.vgui.Create("base_checkbox",_menu.item)
	    _checkbox:SetPos(4,92)
	    _checkbox:SetSize(32,32)
	    _checkbox.text:SetText("///mechanism")
	    _checkbox.button.tggl_status = game.options.audio.mechanism
		function _checkbox.button:OnMouseReleased() game.options.audio.mechanism = self.tggl_status end

	    _checkbox = game.vgui.Create("base_checkbox",_menu.item)
	    _checkbox:SetPos(4,128)
	    _checkbox:SetSize(32,32)
	    _checkbox.text:SetText("///gui")
	    _checkbox.button.tggl_status = game.options.audio.gui
		function _checkbox.button:OnMouseReleased() game.options.audio.gui = self.tggl_status end

	    _checkbox = game.vgui.Create("base_checkbox",_menu.item)
	    _checkbox:SetPos(4,164)
	    _checkbox:SetSize(32,32)
	    _checkbox.text:SetText("///use microphone")
	    _checkbox.button.tggl_status = game.options.audio.gui
		function _checkbox.button:OnMouseReleased() game.options.audio.gui = self.tggl_status end

		_slider = game.vgui.Create("base_slider",_menu.item)
		_slider:SetPos(160,24)
		_slider:SetSize(256,24)
		_slider:SetHorizontal(true)
--[[
game.options = 
{
	video =
	{
		resizable	= true, 
		vsync		= false,
		fullscreen  = false,
		centered	= true,
		srgb		= true,
		borderless  = false,
		highdpi     = false,

		minwidth	= 800, 
		minheight	= 600,
		fsaa		= 0,

		fullscreentype	= "normal", --display
	},
	binds = 
	{
		inventory	= "i",
		drop		= "q",
		pick		= "r",
		use			= "e",
		flashlight	= "f",
		mainmenu    = "escape"
	}

}]]--
	else

		game.gui.list["audio_options"].item.child[3]:OnMouseReleased()

	end


end