math.clamp = function(_var, _min, _max)
		
	if _var >= _min and _var <= _max then 

		return _var

	elseif _var < _min then 

		return _min

	end

	return _max 

end