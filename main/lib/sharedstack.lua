game = game or {}

game.sharedstack =
{
	stack =
	{

	},
	
	status = true,
}

game.sharedstack.SetMode = function(_value)

	game.sharedstack.status = _value

end

game.sharedstack.CheckItem = function(_lvl, _index)
	
	local _result = {true, true}

	if game.sharedstack.stack[_lvl] then 
		
		_result[1] = false
	
	else

		return _result

	end

	if game.sharedstack.stack[_lvl][_index] then
	
		_result[2] = false

	end

	return _result

end

game.sharedstack.CheckLevel = function(_lvl)

	if not game.sharedstack.stack[_lvl] then game.sharedstack.stack[_lvl] = {} end

end

game.sharedstack.PushBack = function(_var, _lvl)
	
	_lvl = _lvl or 1

	game.sharedstack.CheckLevel(_lvl)

	table.insert(game.sharedstack.stack[_lvl], #game.sharedstack.stack[_lvl]+1, _var)

end

game.sharedstack.PopBack = function(_lvl)

	_lvl = _lvl or 1

	game.sharedstack.CheckLevel(_lvl)

	table.remove(game.sharedstack.stack[_lvl], #game.sharedstack.stack[_lvl])	

end

game.sharedstack.Add = function(_var, _lvl, _index)

	_lvl	= _lvl   or 1
	_index	= _index or #game.sharedstack.stack[_lvl]+1

	game.sharedstack.CheckLevel(_lvl)

	table.insert(game.sharedstack.stack[_lvl], _index, _var)

end

game.sharedstack.Remove = function(_var, _lvl, _index)
	
	_lvl	= _lvl or 1
	_index	= _index or #game.sharedstack.stack[_lvl]

	game.sharedstack.CheckLevel(_lvl)

	table.insert(game.sharedstack.stack[_lvl], _index, _var)

end