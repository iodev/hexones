print("String functions")

string.explode = function(str, sep)

	if not sep then	sep = "%s" end
	if type(str) ~= "string" then str = "w-t-f" end
	
	local t = {}

	for str in string.gmatch(str, "([^"..sep.."]+)") do

		table.insert(t,str)

	end

	return t

end

string.left = function(str, count)

	return string.gsub(str, 1, count)

end

string.right = function(str, count)

	return string.gsub(str, -count)

end