print("Table functions")

table.Print = function(_table, _deep)
	
	_deep = _deep or 1

	if _deep > 30 then return end

	local _shift, _line, _end = "", "|>>>|>>>\\", "|<<<|<<</"

	for _id = 1, _deep do	
		
		_shift = "\t|".._shift

		if _id <= _deep-1 then

			_line = "|\t".._line
			_end  = "|\t".._end

		end

	end 

	print("|".._shift)
	print(_line)

	for _id, _obj in pairs(_table) do

		local _str = "|".._shift.."---|"..tostring(_id).." = "..tostring(_obj) --.."\n"

		print(_str)

		if type(_obj) == "table" and tostring(_id) ~= "__index" then

			table.Print(_obj, _deep + 1)

		end
	
	end
	
	print(_end)
	print("|".._shift)

end

table.CountSimple = function(_t)
	
	local _counter = 0

	for _id, _obj in pairs(_t or {}) do

		_counter = _counter + 1

	end

	return _counter

end

table.GetKeyByIndex = function(_t, _index)
	
	local _counter = 0

	for _id, _obj in pairs(_t) do

		_counter = _counter + 1

		if _counter == _index then return _id end

	end 

	return "-"

end

table.FindSimple = function(_table, _value, _maxdeep)

	if _maxdeep > 0 then

		for _id, _obj in pairs(_table) do

			if string.find(tostring(_id),_value,1) then

				print(_id, _obj)

			end

			if type(_obj) == "table" and tostring(_id) ~= "__index" then

				table.FindSimple(_obj, _value, _maxdeep-1)

			end
		
		end

	end

end

table.CopyAdvanced = function(orig)
	
    local orig_type = type(orig)
    local copy

    if orig_type == 'table' then

        copy = {}

        for orig_key, orig_value in next, orig, nil do

            copy[table.CopyAdvanced(orig_key)] = table.CopyAdvanced(orig_value)

        end

        setmetatable(copy, table.CopyAdvanced(getmetatable(orig)))

    else -- number, string, boolean, etc

        copy = orig

    end

    return copy

end

table.MergeAdvanced = function(_t1, _t2)
	
	local _result = {}

	for _id, _obj in pairs(_t2) do

		_t1[_id] = table.CopyAdvanced(_obj)
	
	end

	return _t1

end

table.CopySimple = function(_from)
	
	local _result = {}

	for _id, _obj in pairs(_from) do
			
		_result[_id] = _obj

	end

	return _result

end
