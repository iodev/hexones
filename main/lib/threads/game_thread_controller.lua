
game = game or {}

game.thread = 
{
	threads = {},
}

game.thread.Start = function(_name)

	if game.thread.threads[_name] then game.thread.threads[_name]:start() end

end

game.thread.Stop = function(_name)

	if game.thread.threads[_name] then game.thread.threads[_name]:wait() end

end

game.thread.Create = function(_name, _file)

	if not game.thread.threads[_name] then 

		game.thread.threads[_name] = love.thread.newThread(_file) 
		game.thread.Start(_name)

	end

end

game.thread.Remove = function(_name)

	if game.thread.threads[_name] then 

		game.thread.threads[_name]:wait()
		game.thread.threads[_name] = nil

	end
	
end

game.thread.Send = function(_name, _value)

	if game.thread.threads[_name] then game.thread.send(_name, _value) end

end