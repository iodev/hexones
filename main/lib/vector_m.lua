VECTOR = {}

local VECTOR_META = {}

VECTOR_META.__index = function(table, key)
    
    return table[key]

end

VECTOR_META.__add = function(a,b)

    return vec2(a[1]+b[1], a[2]+b[2])

end

VECTOR_META.__sub = function(a,b)

    return vec2(a[1]-b[1], a[2]-b[2])

end

VECTOR_META.__mul = function(a,b)

    if type(b)=="number" then

        return vec2(a[1]*b, a[2]*b)

    else

        return vec2(a[1]*b[1], a[2]*b[2])

    end

end

VECTOR_META.__div = function(a,b)

    if type(b)=="number" then

        return vec2(a[1]/b, a[2]/b)

    else

        return vec2(a[1]/b[1], a[2]/b[2])

    end

end

function VECTOR:setX(val) 

    self[1] = val

end

function VECTOR:setY(val) 

    self[2] = val

end

function VECTOR:getX()

    return self[1]

end

function VECTOR:getY() 

    return self[2]

end

function VECTOR:distance(vec)

    return math.sqrt((self[1]-vec[1])^2 + (self[2]-vec[2])^2)

end

function vec2(x,y)

    x = x or 0
    y = y or x

    local _result = {}
    
    table.MergeAdvanced(_result, VECTOR)
    setmetatable({[1] = x, [2] = y}, VECTOR_META)

    return _result

end