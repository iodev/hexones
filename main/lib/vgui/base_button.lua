local BUTTON_META = {}
BUTTON_META = table.MergeAdvanced(BUTTON_META, table.CopySimple(game.vgui.types["base_panel"]))
BUTTON_META = table.MergeAdvanced(
	BUTTON_META,
	{
		colors = 
		{
			color(100, 100, 100, 255),
			color(255, 255, 255, 255),
			color(0, 155, 255, 255),
			color(0,0,0,255)
		},
		prev_color  = color(255, 255, 255),
		_status     = 1,
		tggl_status = false,
		is_toggled  = false
	})



function BUTTON_META:OnInit()
	
	self:OnSkin()
	self:SetBorder(true)

end

function BUTTON_META:SetColor(_col1, _col2, _col3, _col4)

	self.colors[1] = _col1 or color(100, 100, 100, 255)
	self.color 	   = self.colors[1]
	self.colors[2] = _col2 or color(255, 255, 255, 255)
	self.colors[3] = _col3 or color(177.5, 177.5, 177.5, 255)
	self.colors[4] = _col4 or color(0, 0, 0, 255)

end

function BUTTON_META:SetToggle(_bool)

	self.is_toggled = type(_bool) == "boolean" and _bool or false

end

function BUTTON_META:GetColorArray()

	return self.colors

end

function BUTTON_META:DoFunction()

	--self:SetPos(self.orig_pos.x+25,0)

end

function BUTTON_META:OnMouseEntered()

end

function BUTTON_META:OnMouseExited()

end

function BUTTON_META:OnMousePressed()

end

function BUTTON_META:OnMouseReleased()

end

function BUTTON_META:OnColor()

	local _color_to = color()

	if not self.is_toggled then

		if self._status == 1 or self._status == 4 then --Exited
			
			if _color_to ~= self.colors[1] then

				_color_to 		= self.colors[1]
		    	self.prev_color = self.color

		    end
		
		elseif self._status == 2 then --Entered

			if _color_to ~= self.colors[2] then

				_color_to 		= self.colors[2]
		    	self.prev_color = self.color

		    end 

		elseif self._status == 3 then --Pressed

			if _color_to ~= self.colors[3] then

				_color_to 		= self.colors[3]
		    	self.prev_color = self.color

		    end		

		end

	else

		if self.tggl_status and _color_to ~= self.colors[3] then

			_color_to 		= self.colors[3]
		    self.prev_color = self.color			

		elseif _color_to ~= self.colors[2] then

			_color_to 		= self.colors[2]
		    self.prev_color = self.color		

		end

	end

	if self.color ~= _color_to then

		self.color      = self.prev_color+(_color_to-self.prev_color)/100
		self.prev_color = self.color

	end

end

function BUTTON_META:OnMouse()

	if (self.global_owner and self.global_owner.is_enabled == true) and self.is_enabled then

		local _in_region = self:MouseInRegion()
		local _l_mouse   = love.mouse.isDown("l")
		
		if not _in_region then --Normal

			if self._status ~= 1 then

				self._status = 1
				self:OnMouseExited()

			end 

		else

			if not _l_mouse then --Entered

				if self._status ~= 4 and self._status == 3 then --Released

					if self.is_toggled then self.tggl_status = not self.tggl_status end

					self._status = 4
					self:OnMouseReleased()
					self:DoFunction()

				elseif self._status ~= 2 then

					self._status = 2
					self:OnMouseEntered()

				end

			elseif _l_mouse and self._status ~= 3 and self._status == 2 then --Pressed

				self._status = 3
				self:OnMousePressed()

			end

		end

		return  

	end

	self._status = 1

end

function BUTTON_META:RequiredTick()

	self:OnMouse()
	self:OnColor()

end

game.vgui.Register("base_button",BUTTON_META)
