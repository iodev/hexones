local CHECKBOX_META = {}
CHECKBOX_META = table.MergeAdvanced(CHECKBOX_META, table.CopySimple(game.vgui.types["base_panel"]))
CHECKBOX_META = table.MergeAdvanced(
	CHECKBOX_META,
	{
		curt_status = 1,
		prev_status = 0,
		offset      = 8,
		is_entered  = false,
		is_usable   = true,
		button 		= nil,
		text 		= nil,
		is_enabled  = true,
	})

function CHECKBOX_META:OnInit()
	
	self:OnSkin()
	self:CreateButton()
	self:CreateText()
	self:SetBorder(false)
	
end

function CHECKBOX_META:CreateText()

	self.text = game.vgui.Create("base_text", self)
	self.text:SetPos(self.orig_size.x,self.orig_size.y/2-self.text.height/2)

end

function CHECKBOX_META:CreateButton()

	local _size   = self.orig_size
	local offset2 = self.offset/2
	self.button = game.vgui.Create("base_button", self)
	self.button:SetSize(_size.x - self.offset,_size.y - self.offset)
	self.button:SetPos(offset2,offset2)
	self.button:SetToggle(true)

end

function CHECKBOX_META:SetSize(_x,_y)

	self:ScaleFactor()
	
	self.orig_size.x = (_x or self.orig_size.x)
	self.orig_size.y = (_y or self.orig_size.y)

	self.glbl_size.x = self.orig_size.x * self.scale.x
	self.glbl_size.y = self.orig_size.y * self.scale.y

	local _size   = self.orig_size
	local offset2 = self.offset/2

	self.button:SetSize(_size.x - self.offset,_size.y - self.offset)
	self.button:SetPos(offset2,offset2)
	self.text:SetPos(self.orig_size.x,self.orig_size.y/2-self.text.height/1.5)
	
end

function CHECKBOX_META:SetOffset(_offset)

	self.offset = _offset
	self:SetSize()

end

function CHECKBOX_META:SetButtonColor(_col1,_col2,_col3)

	self.button:SetColor(_col1,_col2,_col3)

end

function CHECKBOX_META:GetState()

	return self.button.is_toggled

end


game.vgui.Register("base_checkbox",CHECKBOX_META)

