print("Vgui base panel")

local BASE_META = 
{
	color     = color(100,100,100,255),
	colors    = {color(100,100,100,255), color(0,0,0,255)},
	orig_pos  = {x = 0, y = 0},
	glbl_pos  = {x = 0, y = 0},
	scale     = {x = 1, y = 1},
	orig_size = {x = 32, y = 32},
	glbl_size = {x = 32, y = 32},
	size_draw = {0, 0, 32, 0, 32, 32, 0, 32},
	self_id   = 0,
	type	  = "",
	global_owner,
	--is_movable  = true,
	is_enabled  = false,
	is_visible  = true,
}

BASE_META.__index = function(table, key) return table[key] end

function BASE_META:OnInit()
	
	self:OnSkin()
	self:SetBorder(true)

end

function BASE_META:OnSkin()

	if game.vgui.skin.current_skin[self.type] then

		local skin = game.vgui.skin.current_skin[self.type]

		if type(skin) == "table" then 

			self.color = skin[1]

			if self.colors then self.colors = skin end

		end

	end

end

function BASE_META:ScaleFactor()

	self.scale.x = (1/1024)*love.window.getWidth()
	self.scale.y = (1/768)*love.window.getHeight() 

end

function BASE_META:SetVisible(_visible)

	self.is_visible = type(_visible) == "bool" and _visible or true
	self.is_enabled = self.is_visible
	if self.child then

		for _, _obj in pairs(self.child) do

			_obj:SetVisible(self.is_visible)

		end

	end

end

function BASE_META:SetEnabled(_enabled)

	self.is_enabled = type(_enabled) == "bool" and _enabled or true
	
	if self.child then

		for _, _obj in pairs(self.child) do

			if _obj.SetEnabled then _obj:SetEnabled(self.is_enabled) end

		end

	end

end

function BASE_META:SetBorder(_bool)

	self.border = _bool or false

end

function BASE_META:SetColor(_col0, _col1)

	_col0 = _col0 or color(100, 100, 100, 255)
	self.color  = _col0
	self.colors = {_col0, _col1 or _col0}

end

function BASE_META:GetColor()

	return self.color

end

function BASE_META:SetPos(_x, _y)

	self:ScaleFactor()

	self.orig_pos.x = _x and _x or 0
	self.orig_pos.y = _y and _y or 0

	self.glbl_pos   = {x = 0, y = 0}

	if self.owner then 
		
		self.glbl_pos.x = self.owner.glbl_pos.x + self.orig_pos.x * self.scale.x 		
		self.glbl_pos.y = self.owner.glbl_pos.y + self.orig_pos.y * self.scale.y
	
	else

		self.glbl_pos = {x = self.orig_pos.x * self.scale.x, y = self.orig_pos.y * self.scale.y}

	end

	if self.child then

		for _id, _obj in pairs(self.child) do

			_obj:SetPos(_obj.orig_pos.x, _obj.orig_pos.y)

		end
	
	end

end

function BASE_META:GetPos()

	return self.glbl_pos

end

function BASE_META:SetSize(_x,_y)

	self:ScaleFactor()
	
	self.orig_size.x = (_x or self.orig_size.x)
	self.orig_size.y = (_y or self.orig_size.y)

	self.glbl_size.x = self.orig_size.x * self.scale.x
	self.glbl_size.y = self.orig_size.y * self.scale.y

	if self.dragzone then self.dragzone.orig_size.x = self.orig_size.x end

	if self.child then

		for _id, _obj in pairs(self.child) do

			if _obj.SetSize then _obj:SetSize() end

		end
	end

end

function BASE_META:GetSize()

	return self.size

end


function BASE_META:ParentTo(_owner)

	if self.owner and self.owner.child and self.owner.child[self.self_id] then table.remove(self.owner.child, self.self_id) end
	
	if not self.owner       then self.owner   = {} end
	if not _owner.child     then _owner.child = {} end

	self.self_id = #_owner.child + 1

	self.owner   = _owner

	self.is_enabled = _owner.is_enabled

	if _owner.global_owner then 

		self.global_owner = _owner.global_owner
	
	else

		self.global_owner = _owner		

	end

	_owner.child[self.self_id] = self

	if self.is_global then 

		table.remove(game.vgui.table, self.is_global) 
		self.is_global = nil

	end
	
	self:SetPos()

end

function BASE_META:Remove()

	self:OnRemove()

	if self.child then

		for _id, _obj in pairs(self.child) do

			_obj:Remove()

		end
	end

	if self.owner then table.remove(self.owner.child, self.self_id) end
	
	if self.is_global then 
		
		local _count = #game.vgui.table
		
		if self.is_enabled then

			if game.vgui.table[_count-1] and game.vgui.table[_count-1].SetEnabled then

				game.vgui.table[_count-1]:SetEnabled(true)
			
			elseif game.vgui.table[_count+1] and game.vgui.table[_count+1].SetEnabled then

				game.vgui.table[_count-1]:SetEnabled(true)

			end

		end

		table.remove(game.vgui.table, self.is_global)

		for i = 1, #game.vgui.table do game.vgui.table[i].is_global = i end

	end

end

function BASE_META:MouseInRegion()

	local _x, _y = game.environment.varargs.mouse_pos[1], game.environment.varargs.mouse_pos[2]
	local _pos   = self.glbl_pos

	return (_x > _pos.x and _x < (_pos.x + self.glbl_size.x) and 
	   		_y > _pos.y and _y < (_pos.y + self.glbl_size.y))

end

function BASE_META:OnRemove()


end

function BASE_META:OnPaint()

	love.graphics.setColor(self.color)
	love.graphics.rectangle("fill", self.glbl_pos.x, self.glbl_pos.y, self.glbl_size.x, self.glbl_size.y)

	if self.border then 

		love.graphics.setColor(self.colors[4] or self.colors[2])
		love.graphics.rectangle("line", self.glbl_pos.x, self.glbl_pos.y, self.glbl_size.x, self.glbl_size.y)

	end

end

function BASE_META:OnThink()

end

function BASE_META:RequiredTick()



end 

function BASE_META:OnTick()

	self:OnThink()
	self:RequiredTick()

	if self.child then

		for _id, _obj in pairs(self.child) do
			
			_obj:OnTick()

		end

	end

end

function BASE_META:OnDraw()

	if self.is_visible then

		self:OnPaint()

		if self.child then

			for _id, _obj in pairs(self.child) do
			
				_obj:OnDraw()

			end

		end

	end

end

game.vgui.Register("base_panel",BASE_META)
