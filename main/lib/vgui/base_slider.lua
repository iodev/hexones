local SLIDER_META = {}
SLIDER_META = table.MergeAdvanced(SLIDER_META, table.CopySimple(game.vgui.types["base_panel"]))
SLIDER_META = table.MergeAdvanced(
	SLIDER_META,
	{
		is_resizable  = false,
		is_draggable  = false,
		knob_size 	  = 32,
		dragged       = true,
		prev_dragged  = false,
		prev_clicked  = false,
		is_horizontal = false,
		state	 	  = 0,
		mouse_offset  = {x = 0, y = 0},
		knob 		  = nil,
		prev_knob_pos = 0,
	})


function SLIDER_META:OnInit()

	self:CreateKnob()

end


function SLIDER_META:CreateKnob()
	
	self.knob = game.vgui.Create("base_button", self)
	
	self.knob:SetPos(0,0)

	if self.is_horizontal then

		self.knob:SetSize(self.knob_size,self.orig_size.y)

	else

		self.knob:SetSize(self.orig_size.x,self.knob_size)

	end

	function self.knob:OnMousePressed()

		self.owner.dragged = true

	end

end

function SLIDER_META:SetKnobColor(_col1,_col2,_col3,_col4)

	self.knob:SetColor(_col1, _col2, _col3,_col4)

end





function SLIDER_META:SetHorizontal(_is_horizontal)

	self.is_horizontal = _is_horizontal

	if self.knob then 

		self.knob:Remove() 
		self:CreateKnob()

	end

end


function SLIDER_META:SetKnobSize(_size)

	self.knob_size = _size

	if self.knob then 

		self.knob:Remove() 
		self:CreateKnob()

	end

end

function SLIDER_META:GetState()

	return self.out

end

function SLIDER_META:RequiredTick()

	if self.dragged then

		local _m_pos   = game.environment.varargs.mouse_pos

		if not self.prev_dragged then
			
			self.mouse_offset.x = _m_pos[1] - self.knob.glbl_pos.x
			self.mouse_offset.y = _m_pos[2] - self.knob.glbl_pos.y
			
			self.prev_dragged = true

		end

		if self.is_horizontal then
		
			self.knob:SetPos(math.clamp(_m_pos[1]-self.mouse_offset.x-self.glbl_pos.x,0,self.glbl_size.x-self.knob.glbl_size.x),0)
			self.state = self.knob.orig_pos.x/(self.glbl_size.x-self.knob.glbl_size.x)
			self.prev_knob_pos = self.knob.orig_pos.x
		else

			self.knob:SetPos(math.clamp(_m_pos[2]-self.mouse_offset.y-self.glbl_pos.y,0,self.glbl_size.x-self.knob.glbl_size.y),0)
			self.state = self.knob.orig_pos.y/(self.glbl_size.y-self.knob.glbl_size.y)
			self.prev_knob_pos = self.knob.orig_pos.y
		end

		if not love.mouse.isDown("l") then

			self.dragged = false

		end
		

	elseif self.prev_dragged then

		self.mouse_offset.x = 0
		self.mouse_offset.y = 0
		self.prev_dragged = false

	else
		
		local _pos_to = nil

		if self:MouseInRegion() and love.mouse.isDown("l") then

			local _m_pos   = game.environment.varargs.mouse_pos
			
			if self.is_horizontal then
							
				_pos_to = math.clamp(_m_pos[1]-self.knob_size/2,0,self.glbl_size.x-self.knob.glbl_size.x)

			else
							
				_pos_to = math.clamp(_m_pos[2]-self.knob_size/2,0,self.glbl_size.y-self.knob.glbl_size.y)
						
			end

		end

		if _pos_to and self.knob.orig_pos ~= _pos_to then

			if self.is_horizontal then

				self.knob:SetPos(self.prev_knob_pos+(_pos_to-self.prev_knob_pos)/100,0)
				self.prev_knob_pos = self.knob.orig_pos.x

			else

				self.knob:SetPos(0,self.prev_knob_pos+(_pos_to-self.prev_knob_pos)/100)
				self.prev_knob_pos = self.knob.orig_pos.y

			end

		end

	end

end

game.vgui.Register("base_slider",SLIDER_META)

