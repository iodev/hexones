local TEXT_META = 
{

	color = color(0,0,0),
	owner = nil,
	orig_pos   = {x = 0, y = 0},
	glbl_pos   = {x = 0, y = 0},
	align_x    = 0,
	is_visible = true,
	text = "",
	font_name = "Default",
	font = nil,
	scale = 0,
	size = 32,

}


TEXT_META.__index = function(table, key) return table[key] end

function TEXT_META:OnInit()

	self.font = love.graphics.newFont(game.vgui.fonts[self.font_name],self.size)

end

function TEXT_META:SetVisible(_visible)

	self.is_visible = _visible

end

function TEXT_META:SetColor(_col)

	self.color  = _col or color(100, 100, 100, 255)

end


function TEXT_META:GetColor()

	return self.color

end

function TEXT_META:SetPos(_x, _y)

	self:ScaleFactor()

	self.orig_pos.x = _x or self.orig_pos.x or 0
	self.orig_pos.y = _y or self.orig_pos.y or 0
	
	self.glbl_pos   = {x = 0, y = 0}

	if self.owner then 
		
		self.glbl_pos.x = self.owner.glbl_pos.x + self.orig_pos.x - (self.font:getWidth(self.text) * self.align_x)
		self.glbl_pos.y = self.owner.glbl_pos.y + self.orig_pos.y * self.scale

	else

		self.glbl_pos = self.orig_pos

	end


end

function TEXT_META:ScaleFactor()

	self.scale = (1/768)*love.window.getHeight() 

end

function TEXT_META:SetSize(_s)

	self:ScaleFactor()
	self.size = _s or self.size
	self.font = love.graphics.newFont(game.vgui.fonts[self.font_name],self.size * self.scale)

end

function TEXT_META:GetSize()

	return self.size

end

function TEXT_META:GetPos()

	return self.glbl_pos

end

function TEXT_META:SetAlign(_x)

	self.align_x = _x
	self:SetPos()

end

function TEXT_META:SetText(_text)

	self.text = _text
	self:SetPos()

end

function TEXT_META:GetText()

	return self.text

end

function TEXT_META:SetFont(_font)

	self.font_name = _font
	self.font = love.graphics.newFont(game.vgui.fonts[self.font_name],self.size)

end

function TEXT_META:GetFont()

	return self.font

end

function TEXT_META:OnTick()

end

function TEXT_META:ParentTo(_owner)

	if self.owner and self.owner.child and self.owner.child[self.self_id] then table.remove(self.owner.child, self.self_id) end
	
	if not self.owner       then self.owner   = {} end
	if not _owner.child     then _owner.child = {} end

	self.self_id = #_owner.child + 1

	self.owner   = _owner

	if _owner.global_owner then 

		self.global_owner = _owner.global_owner
	
	else

		self.global_owner = _owner		

	end

	_owner.child[self.self_id] = self

	if self.is_global then 

		table.remove(game.vgui.table, self.is_global) 
		self.is_global = nil

	end
	
	self:SetPos()

end

function TEXT_META:Remove()

	if self.owner then table.remove(self.owner.child, self.self_id) end
	
	if self.is_global then 
		
		if game.vgui.table[self.is_global+1] then 
			
			game.vgui.table[self.is_global+1]:SetEnabled(true)
			
		elseif game.vgui.table[self.is_global-1] then

			game.vgui.table[self.is_global-1]:SetEnabled(true)

		end
		
		table.remove(game.vgui.table, self.is_global)

		for i = 1, #game.vgui.table do game.vgui.table[i].is_global = i end

	end

end

function TEXT_META:MouseInRegion()

	local _x, _y = game.environment.varargs.mouse_pos[1], game.environment.varargs.mouse_pos[2]
	local _pos   = self.glbl_pos

	return (_x > _pos.x and _x < (_pos.x + self.font:getWidth(self.text)) and 
	   		_y > _pos.y and _y < (_pos.y + self.font:getHeight()))

end

function TEXT_META:OnDraw()

	if self.is_visible then
		
		love.graphics.setFont(self.font)
		love.graphics.setColor(self.color)
		love.graphics.print(self.text, self.glbl_pos.x, self.glbl_pos.y)

	end

end

game.vgui.Register("base_text",TEXT_META)