local WINDOW_META = {}
WINDOW_META = table.MergeAdvanced(WINDOW_META, table.CopySimple(game.vgui.types["base_panel"]))
WINDOW_META = table.MergeAdvanced(
	WINDOW_META,
	{
		is_resizable  = false,
		is_draggable  = false,
		is_dragged 	  = false,
		prev_dragged  = false,
		dragzone_size = {x=32,y=32},
		mouse_offset  = {x=0,y=0},
	})

function WINDOW_META:SetDraggable(_drag, _size1, _size2)
	
	self:ScaleFactor()

	if _drag then

		self.dragzone_size.x = (_size2 and _size1 or self.orig_size.x)
		self.dragzone_size.y = (_size2 or _size1)
		
		if not self.is_draggable then
			
			self.dragzone = game.vgui.Create("base_button", self)

		end

		self.dragzone:SetSize(self.dragzone_size.x,self.dragzone_size.y)

	elseif self.is_draggable == false and self.dragzone then

		self.dragzone:Remove()

	end

	self.is_draggable = _drag

end
--[[
function WINDOW_META:MouseInRegion()

	local _x, _y = love.mouse.getPosition()
	local _pos   = self.glbl_pos
	return (_x > _pos.x and _x < (_pos.x + self.size.x) and 
	   		_y > _pos.y and _y < (_pos.y + self.dragzone_y))

end
]]--

function WINDOW_META:RequiredTick()

	if (self.global_owner and self.global_owner.is_enabled) or self.is_enabled then

		if self.dragzone then

			if self.dragzone._status == 3 then

				local m_x, m_y = game.environment.varargs.mouse_pos[1]/self.scale.x, game.environment.varargs.mouse_pos[2]/self.scale.y

				if not self.prev_dragged then
					
					self.mouse_offset.x = (m_x - self.glbl_pos.x/self.scale.x)
					self.mouse_offset.y = (m_y - self.glbl_pos.y/self.scale.y)
					
					self.prev_dragged = true 

				end
				
				self:SetPos(m_x-self.mouse_offset.x,m_y-self.mouse_offset.y)

			elseif self.prev_dragged then

				self.mouse_offset.x = 0
				self.mouse_offset.y = 0
				self.prev_dragged = false

			end

		end

	end	

end 



game.vgui.Register("base_window",WINDOW_META)