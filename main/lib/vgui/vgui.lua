print("Vgui register functions")

game = game or {}

game.vgui = 
{
	types = {},
	fonts = {},
	table = {},
}

game.vgui.CreateFont = function(_name, _font, _size)

	game.vgui.fonts[_name] = love.graphics.newFont(_font, _size)

end

game.vgui.Register = function(_type, _table)
	
	local _result = {}
	
	game.vgui.types[_type] = table.MergeAdvanced(_result, _table)
	game.vgui.types[_type].type = _type

end

game.vgui.Create = function(_type, _parent)
	
	if game.vgui.types[_type] then

		local _object      = {}
		local _table_index = #game.vgui.table+1

		table.MergeAdvanced(_object, game.vgui.types[_type])

		_object:OnInit()

		if not _parent then

			_object.is_global = _table_index

			if game.vgui.table[_table_index-1] then game.vgui.table[_table_index-1].is_enabled = false end

			_object.is_enabled = true
			table.insert(game.vgui.table, _object)

		else

			_object:ParentTo(_parent)

		end
		
		return _object

	else

		game.debug.Message("VGUI Type: [".._type.."] Not Exists")

		return {}

	end

end

game.vgui.OnDraw = function()
	
	for _id, _obj in pairs(game.vgui.table) do

		if _obj.is_visible then

			_obj:OnDraw()

		end

	end	

end

game.vgui.OnTick = function()

	for _id, _obj in pairs(game.vgui.table) do

		_obj:OnTick()

	end		

end

game.vgui.OnDeep = function()
	
	local _count = #game.vgui.table
	
	if _count > 1 then
		
		if game.vgui.table[#game.vgui.table] then
			
			local _last_object = game.vgui.table[#game.vgui.table]

			if not _last_object:MouseInRegion() and not _last_object.is_dragged then

				for _id = #game.vgui.table-1, 1, -1 do

					local _obj = game.vgui.table[_id]

					if _obj:MouseInRegion() then --Это говно срочно нужно чинить. Ибо кнопки за панелями активны.
						
						game.vgui.table[#game.vgui.table].is_enabled = false
						_obj.is_enabled = true

   						table.remove(game.vgui.table, _id)
   						table.insert(game.vgui.table, _obj)

						for i = 1, #game.vgui.table do game.vgui.table[i].is_global = i end

   						break

					end

				end

			end

		end

	end

end