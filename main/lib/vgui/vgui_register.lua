print("Vgui register manager")

require "lib/vgui/vgui"
require "lib/vgui/vgui_skin"
require "lib/vgui/skin_register"
require "lib/vgui/font_register"
require "lib/vgui/base_element"
require "lib/vgui/base_text"
require "lib/vgui/base_button"
require "lib/vgui/base_window"
require "lib/vgui/base_slider"
require "lib/vgui/base_checkbox"