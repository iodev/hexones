game.vgui.skin = 
{
	current_skin = {},
	skins = {}
}

game.vgui.skin.Create = function(_name, _type, _colors)

	if not game.vgui.skin.skins[_name] then game.vgui.skin.skins[_name] = {} end
	if not game.vgui.skin.skins[_name][_type] then game.vgui.skin.skins[_name][_type] = {} end

	game.vgui.skin.skins[_name][_type] = _colors

end

game.vgui.skin.Enabled = function(_name)
	
	if not game.vgui.skin.skins[_name] then

		game.debug.Message("VGUI Skin: [".._name.."] Skin Not Exists")

	end

	game.vgui.skin.current_skin = {}
	game.vgui.skin.current_skin = table.MergeAdvanced(game.vgui.skin.current_skin, game.vgui.skin.skins[_name])

end