require "lib/threads/game_thread_controller"
require "lib/debug"
require "lib/color_m"
require "lib/math_m"
require "lib/string_m"
require "lib/table_m"
require "lib/sharedstack"
require "lib/environment"
require "lib/vgui/vgui_register"
require "data/options"
require "lib/gui/gui_menu"

game = game or {}

game.managers =
{
	game_status = 0,
}

game.UpdateArgs = function()
	
	game.environment.varargs.mouse_pos  = {love.mouse.getPosition()}
	game.environment.varargs.mouse_left = love.mouse.isDown("l")

end

game.SafeLoadFile = function(_path)

	local _status, _chunk, _result

	_status, _chunk = pcall( love.filesystem.load, _path )

	if not _status then

		print('Error: ' .. tostring(_chunk))
		return

	else

		_status, _result = pcall(_chunk)

		if not _status then

			print('Error: ' .. tostring(_result))
			return

		else

			print('Error: ' .. tostring(_result))
			return

		end

		return _result

	end

end

game.SafeLoadImage = function(_path)

	return love.graphics.newImage(_path)

end

game.SafeLoadSound = function(_path)
	
	return love.sound.newSoundData(_path)

end

game.GetFileType = function(_path)

	local _filename, _filetype = string.match(_path,"(.+)%.(.-)$")

	return _filetype, _filename

end

game.QueueLoad = function(_path)
	
	local _prepare  = string.explode(_path, "/")
	local _sub_type = string.explode(_prepare[#_prepare], "-")
	local _name 	= string.explode(_sub_type[2], ".")[1]
	local _type 	= game.GetFileType(_path)

	if not game.environment.resources.textures[_sub_type[1]][_name] then 

		game.environment.resources.textures[_sub_type[1]][_name] = {}

	end

	if _type == "png" or _type == "jpg" or _type == "bmp" or _type == "tga" then

		game.environment.resources.textures[_sub_type[1]][_name] = game.SafeLoadImage(_path)

	elseif _type == "lua" then

		game.environment.resources.lua[_sub_type[1]][_name] = game.SafeLoadFile(_path)
	
	elseif _type == "mp3" or _type == "ogg" then

		game.environment.resources.sounds[_sub_type[1]][_name] = game.SafeLoadFile(_path)

	elseif _type == "shader" then

		game.environment.resources.shaders[_sub_type[1]][_name] = game.SafeLoadFile(_path)

	else

		game.environment.resources.others[_sub_type[1]] = game.SafeLoadFile(_path)

	end

end

game.LoadData = function(_path)

    local _lfs = love.filesystem
    local _data = _lfs.getDirectoryItems(_path)

    for _id, _obj in ipairs(_data) do

        local _file = _path.."/".._obj

        if _lfs.isFile(_file) then

        	game.QueueLoad(_file)

        elseif _lfs.isDirectory(_file) then

            game.LoadData(_file)

        end

    end

end

game.Init = function()

	local l_cSource = love.image.newImageData("GameIcon.png")

	game.GetFileType("GameIcon.png")

	love.window.setIcon(l_cSource)
	love.window.setTitle("Hexones")

	love.window.setMode(game.options.video.minwidth, game.options.video.minheight, game.options.video)

	game.LoadData("resource")

	game.managers.game_status = 1

	--game.vgui.skin.Create("Default","base_window","base_window")
	--game.vgui.skin.Enabled("Default")

	table.Print(game.environment.resources)
	fps = game.vgui.Create("base_text")
	fps:SetFont("Default")
	fps:SetColor(color(0,0,255))
	fps:SetSize(28)
	fps:SetPos(600,40)


end

game.Init()

local function gen_planet_chunks(Rad, Div, Sec)

	local _result = {}
	local cos, sin, rad = math.cos, math.sin, math.rad
	
	for _id0 = 1, Div do

		if not _result[_id0] then _result[_id0] = {} end

		local F = 360/Div
		local F0 = F*_id0
		local F1 = F*(_id0+1)

		for _id1 = 1, Sec do

			local SF = Rad/Sec
			local S0 = SF*_id1
			local S1 = SF*(_id1+1)
			
			local x1,x2,x3,x4
			local y1,y2,y3,y4

			x1 = cos(rad(F0))*S1
			x2 = cos(rad(F0))*S0
			x3 = cos(rad(F1))*S0
			x4 = cos(rad(F1))*S1

			y1 = sin(rad(F0))*S1
			y2 = sin(rad(F0))*S0
			y3 = sin(rad(F1))*S0
			y4 = sin(rad(F1))*S1

			_result[_id0][_id1] = {x1,y1,x2,y2,x3,y3,x4,y4}

		end

	end

	return _result

end

local wtf = gen_planet_chunks(64, 4, 12)
local meshes = {}
local lp = {512,512}

    for _i0, _obj0 in pairs(wtf) do
    	
    	meshes[_i0] = {}

    	for _i1, _obj1 in pairs(_obj0) do
    		
    		local r = _i1*25
    		meshes[_i0][_i1] = {}
    		meshes[_i0][_i1] = love.graphics.newMesh(
    			{
	    			{
	    				_obj1[1],_obj1[2],
	    				0, 0,
	    				0, r, r/2, 255,
	    			},
	    			{
	    				_obj1[3],_obj1[4],
	    				1, 0,
	    				0, r, r/2, 255,
	    			},
	    			{
	    				_obj1[5],_obj1[6],
	    				1, 1,
	    				0, r, r/2, 255,
	    			},
	    			{
	    				_obj1[7],_obj1[8],
	    				0, 1,
	    				0, r, r/2, 255,
	    			}
	    		},nil,"fan")

	    end

    end

function love.draw()

	game.vgui.OnDraw()
    -- draw things
    love.graphics.setPointSize(4)
    for _i0, _obj0 in pairs(meshes) do
    	
    	for _i1, _obj1 in pairs(_obj0) do

    		local F = love.timer.getTime()*50
    		love.graphics.draw(_obj1, 256+math.cos(math.rad(F))*200, 256+math.sin(math.rad(F))*200)

    	end

    end

end

function love.update()
	
	--l1:SetPos(100+x,100+y)
	--l1:SetSize(256+x,256+y)
	--l2:SetPos(100+x,100+y)
	--l3:SetPos(100+x,100+y)
	
	--print(tostring(game.vgui.namespace_array))
	
	game.UpdateArgs()
	game.vgui.OnTick()
	game.vgui.OnDeep()

	fps:SetText("FPS:"..tostring(love.timer.getFPS()))

end

function love.quit()

	--table.Print(game.vgui.table)

end


function love.keyreleased(key)

	if key == "escape" then

		if game.gui.crnt ~= "main_menu" and game.gui.list[game.gui.crnt] then
			
			if game.gui.list[game.gui.crnt].menu.child[#game.gui.list[game.gui.crnt].menu.child].child[1].text == "Back" then

				game.gui.list[game.gui.crnt].menu.child[#game.gui.list[game.gui.crnt].menu.child]:OnMouseReleased()

			end

		else

			game.gui.Use("main_menu")
		
		end
	
	end

end

function love.resize(x,y)

	for _id, _obj in pairs(game.vgui.table) do

		if _obj.SetSize then 

			_obj:SetPos(_obj.orig_pos.x, _obj.orig_pos.y) 
			_obj:SetSize()

		end

	end	

end

function love.run()

    if love.math then
        love.math.setRandomSeed(os.time())
        for i=1,3 do love.math.random() end
    end

    if love.event then
        love.event.pump()
    end

    if love.load then love.load(arg) end

    -- We don't want the first frame's dt to include time taken by love.load.
    if love.timer then love.timer.step() end

    local dt = 0

    -- Main loop time.
    while true do
        -- Process events.
        if love.event then
            love.event.pump()
            for e,a,b,c,d in love.event.poll() do
                if e == "quit" then
                    if not love.quit or not love.quit() then
                        if love.audio then
                            love.audio.stop()
                        end
                        return
                    end
                end
                love.handlers[e](a,b,c,d)
            end
        end

        -- Update dt, as we'll be passing it to update
        if love.timer then
            love.timer.step()
            dt = love.timer.getDelta()
        end

        -- Call update and draw
        if love.update then love.update(dt) end -- will pass 0 if love.timer is disabled

        if love.window and love.graphics and love.window.isCreated() then
            love.graphics.clear()
            love.graphics.origin()
            if love.draw then love.draw() end
            love.graphics.present()
        end

        if love.timer then love.timer.sleep(0) end
    end

end